﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HasLaserOnObject : MonoBehaviour {
    bool startMoving = false;
    public GameObject Laser;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (startMoving == true) {
            Laser.transform.Translate(0.0f, 0.0f, -50.0f * Time.deltaTime);
        }
	}

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Player")
        {
            Laser.SetActive(true);
            startMoving = true;
        }
    }
}
