﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : MonoBehaviour {
    public float playerTransformSpeed = 5.0f;
    public float Jumpforce = 100.0f;
    public float DefaultJumpForce = 2680.0f;
    public float MassDefault = 0.15f;
    public float BigSize = 100.0f;
    public float DefaultSize = 20.0f;
    public float SmallSize = 1.0f;
    public float DefaultDrag = 1.0f;
    public float DefaultSpeed = 0.25f;
    public float DefaultRunSpeed = 0.35f;
    public int DefaultFieldOfView = 60;
    public static int checkPoint = 0;
    public static Vector3 spawnPosition;
    public Vector3 OriginalSpawnPosition;

    public int jumplimit = 0;

    public GameObject camera;
    public GameObject BlobShadow;
    public GameObject VictoryUI;
    public GameObject StartingPoint;
    public GameObject GameManager;
    float PositionBeforeJumping = 0;
    float sizeBeforeJumping = 0;
    [SerializeField] float m_GroundCheckDistance = 0.1f;
    float m_OrigGroundCheckDistance;
    bool isGround = false;
    bool OnAir = false;
    bool FPS = false;
    bool jumping = false;
    Vector3 m_GroundNormal;
    public LayerMask GroundLayers;

    public CameraFollow CamFollow;
    Rigidbody rb;
    AudioSource m_MyAudioSource;
    public float checkBoxDistance = 0.1f;
    public float checkBoxRadius = 0.5f;
    bool moving;
    Collider BoxCollider;

    // Use this for initialization
    void Start()
    {
        this.transform.localScale = new Vector3(DefaultSize, DefaultSize, DefaultSize);
        Jumpforce = DefaultJumpForce;
        m_OrigGroundCheckDistance = m_GroundCheckDistance;

        if (checkPoint == 0)
        {
            gameObject.transform.position = OriginalSpawnPosition;
        }
        else
        {
            gameObject.transform.position = spawnPosition;
        }
        Time.timeScale = 1.0f;
        rb = GetComponent<Rigidbody>();
        m_MyAudioSource = GetComponent<AudioSource>();
        camera = GameObject.Find("PlayerCamera");
        GameManager = GameObject.Find("GameManager");
        BoxCollider = GetComponent<Collider>();
        CamFollow = GameObject.FindObjectOfType<CameraFollow>();
    }

    public void respawn()
    {
        this.transform.localScale = new Vector3(DefaultSize, DefaultSize, DefaultSize);
        Jumpforce = DefaultJumpForce;
        if (checkPoint == 0)
        {
            gameObject.transform.position = OriginalSpawnPosition;
        }
        else
        {
            gameObject.transform.position = spawnPosition;
        }
        Time.timeScale = 1.0f;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (this.transform.localScale.x <= 20)
        {
            Jumpforce = 1500.0f;
        }
        else
        {
            Jumpforce = (this.transform.localScale.x * 100);
        }


        if (isGround == false)
        {
        //    Debug.Log((transform.position.y - StartingPoint.transform.position.y) - transform.localScale.y);
        }
        BlobShadow.GetComponent<Projector>().farClipPlane = this.transform.localScale.x * 1000;
        ChangeState();
        if (this.transform.localScale.x <= DefaultSize)
        {
            camera.GetComponent<CameraFollow>().offset.z = 5.71f * this.transform.localScale.x;
            camera.GetComponent<CameraFollow>().offset.y = 4.15f * this.transform.localScale.x;
        }
        else
        {
            camera.GetComponent<CameraFollow>().offset.z = (5.71f / 1.75f) * this.transform.localScale.x;
            camera.GetComponent<CameraFollow>().offset.y = 2.15f * this.transform.localScale.x;

        }

        GameManager.GetComponent<GameManager>().ObjectMovingTime = DefaultSize / this.transform.localScale.x;
        if (this.transform.localScale.x < DefaultSize)
        {
            rb.drag = (DefaultSize - this.transform.localScale.x) * 0.15f;
        }
        else
        {
            rb.drag = 0.0f;
        }
        MovePlayer();
        Jump();
        if (this.transform.localScale.x >= 20)
        {
            if ((transform.position.y - PositionBeforeJumping) - transform.localScale.y >= sizeBeforeJumping + 10.0f)
            {
                rb.AddForce(-(Jumpforce / 15.0f) * Vector2.up);
            }
        }
        else {
            if ((transform.position.y - PositionBeforeJumping) - transform.localScale.y >= sizeBeforeJumping + 20.0f)
            {
                rb.AddForce(-(Jumpforce / 17.5f) * Vector2.up);
            }
        }


        CamPerspective();

    }



    void ChangeState()
    {
        if (Input.GetMouseButton(0))
        {

            if (this.transform.localScale.y <= SmallSize)
            {
                this.transform.localScale = new Vector3(SmallSize, SmallSize, SmallSize);
            }
            else
            {
                //Jumpforce -= 60.0f;
                //if (Time.timeScale > 0)
                //{
                //    Time.timeScale -= 0.035f;
                //}
                //else
                //{
                //    Time.timeScale = 0.01f;
                //}
                //if (this.transform.localScale.x < DefaultSize) {
                //    rb.drag += 0.05f;
                //}

                this.transform.localScale -= new Vector3(1.0f, 1.0f, 1.0f);
            }
        }
        if (Input.GetMouseButton(1))
        {

            if (this.transform.localScale.y >= BigSize)
            {
                this.transform.localScale = new Vector3(BigSize, BigSize, BigSize);
            }
            else
            {
                //Jumpforce += 60.0f;
                // Time.timeScale += 0.035f;
                //if (rb.drag > 0)
                //{
                //    rb.drag -= 0.035f;
                //}
                //else {
                //    rb.drag = 0;
                //}

                this.transform.localScale += new Vector3(1.0f, 1.0f, 1.0f);
            }


        }

    }

    void Jump()
    {
        if (isGround == true)
        {
            jumplimit = 0;
        }
        if (Input.GetKeyDown(KeyCode.Space) && jumplimit<2)
        {
            print(jumplimit + "jumps");
            PositionBeforeJumping = transform.position.y;
            sizeBeforeJumping = transform.localScale.y;
            m_MyAudioSource.Stop();
            FindObjectOfType<SoundManager>().Play("Jump");
            isGround = false;
            rb.AddForce(Jumpforce * Vector2.up);
            jumplimit++;
        }

        else
        {

        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            rb.AddForce(-Jumpforce * Vector2.up);
        }
    }

    void CamPerspective()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (!FPS)
            {
                camera.transform.localPosition = new Vector3(camera.transform.localPosition.x, camera.transform.localPosition.y, -3.76f);
                FPS = true;
            }
            else
            {
                camera.transform.localPosition = new Vector3(camera.transform.localPosition.x, camera.transform.localPosition.y, 0f);
                FPS = false;
            }
        }
    }

    void MovePlayer()
    {

        float Speed = DefaultSpeed;
        if (Input.GetKey(KeyCode.LeftShift))
        {
            Speed = DefaultRunSpeed;
        }

        float horiz = Input.GetAxis("Horizontal");
        float vert = Input.GetAxis("Vertical");
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
        {
            //if (m_MyAudioSource.isPlaying == false && isGround && !OnAir)
            //{
            //    m_MyAudioSource.Play();
            //}

        }
        if (isGround == false)
        {
            transform.Translate(horiz * (Speed / 2.0f) * (Jumpforce / 1500.0f), 0, vert * (Speed / 2.0f) * (Jumpforce / 1500.0f));
        }
        else
        {
            transform.Translate(horiz * Speed, 0, vert * Speed);
        }


    }


    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Ground")
        {
            isGround = true;
            OnAir = false;
        }

        if (col.gameObject.tag == "Laser" || col.gameObject.tag == "DeadlyFloor" || col.gameObject.tag == "BreakableWhenBig")
        {
            gameObject.SetActive(false);
        }
        if (col.gameObject.tag == "Goal")
        {
            VictoryUI.SetActive(true);
        }
        if (col.gameObject.tag == "MovingPlatform")
        {
            transform.parent = col.transform;
        }

    }

    void OnCollisionStay(Collision col)
    {


        if (col.gameObject.tag == "Goal")
        {
            VictoryUI.SetActive(true);
            OnAir = false;
        }

        if (col.gameObject.tag == "CheckPoint")
        {
            isGround = true;
            OnAir = false;
            checkPoint = 1;
            spawnPosition = col.gameObject.transform.position;
            spawnPosition.y += 200.0f;
        }




    }

    void OnCollisionExit(Collision col)
    {
        if (col.gameObject.tag == "Ground")
        {
            OnAir = true;
        }
        if (col.gameObject.tag == "MovingPlatform")
        {
            transform.parent = null;
        }
    }
}
