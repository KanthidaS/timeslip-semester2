﻿using UnityEngine;
using System;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour {
    public soundClass[] Sounds;
	// Use this for initialization
	void Awake () {
        foreach (soundClass s in Sounds)
        {
            s.source=gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.vol;
            s.source.pitch = s.pitch;
            s.source.loop = s.Loop;
            s.source.playOnAwake = s.PlayOnAwake;
        }
		
	}

    public void changePitch(string name, float pitch) {
        soundClass s = Array.Find(Sounds, Sound => Sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found...");
            return;
        }
        if (pitch >= 3.0f) {
            pitch = 3.0f;
        }
        s.source.pitch = pitch;
    }

    public void Play(string name)
    {

        soundClass s = Array.Find(Sounds , Sound => Sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: "+ name +" not found...");
            return;
        }
        s.source.Play();
    }

    public void Play2(string name)
    {

        soundClass s = Array.Find(Sounds, Sound => Sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found...");
            return;
        }
        if (s.source.isPlaying == false) {
            s.source.Play();
        }
        
    }

    public void Stop(string name)
    {

        soundClass s = Array.Find(Sounds, Sound => Sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found...");
            return;
        }
        s.source.Stop();
    }
}
