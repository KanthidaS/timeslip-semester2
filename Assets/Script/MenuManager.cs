﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {
    public GameObject PlayButton, AbilityButton, EditorButton, SettingsButton, QuitButton;
    public GameObject ComingSoon;
    public GameObject SettingPanel;
    // Use this for initialization
    void Start () {
        ComingSoon.SetActive(false);
        FindObjectOfType<SoundManager>().Play("MenuBGM");
    }
	
	// Update is called once per frame

    public void PlayPress()
    {
        SceneManager.LoadScene("StageSelectScene");
    }
    public void AbilityPress()
    {
        StartCoroutine(comingCountDown());
    }
    public void EditorPress()
    {
        StartCoroutine(comingCountDown());
    }
    public void SettingsPress()
    {
        StartCoroutine(comingCountDown());
    }
    public void QuitPress()
    {
        Application.Quit();
    }



    IEnumerator comingCountDown()
    {
        ComingSoon.SetActive(true);
        yield return new WaitForSeconds(1.0f);
        ComingSoon.SetActive(false);
    }

}
