﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StageSelect : MonoBehaviour {
    int currentStage = 0;
    public GameObject [] StagePictures;
    public GameObject[] StageText;
    public GameObject PlayBTN;
    // Use this for initialization
    void Start () {
        PlayBTN.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void selectStage(int numStage) {
        PlayBTN.SetActive(true);
        currentStage = numStage;
        StagePictures[numStage - 1].SetActive(true);
        StageText[numStage - 1].SetActive(true);
        for (int i = 0; i < StagePictures.Length; i++) {
            if (i != numStage - 1) {
                StagePictures[i].SetActive(false);
                StageText[i].SetActive(false);
            }
        }
    }

    public void start() {
        if (currentStage == 1) {
            SceneManager.LoadScene("Stage 1-1");
        } else if (currentStage == 2) {
            SceneManager.LoadScene("Stage 1-2");
        }
    }

    public void MainMenu() {
        SceneManager.LoadScene("MenuScene");
    }
}
