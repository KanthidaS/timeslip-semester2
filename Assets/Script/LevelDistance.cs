﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class LevelDistance : MonoBehaviour
{
    [Header("World Object")]
    public GameObject PlayerPos;
    public GameObject path1;
    public GameObject DestinationPos;
    public float LvDistance;
    public float currentDistance;
    public float percentage;



    // Use this for initialization
    void Start()
    {
        //uiPercentage = ;
        LvDistance = Vector3.Distance(path1.transform.position, DestinationPos.transform.position);
       // print("distance is" + LvDistance);

    }

    // Update is called once per frame
    void Update()
    {
        DistanceCalculator();
    }

    void DistanceCalculator()
    {
        percentage = 1f-(currentDistance / LvDistance);
        currentDistance = Vector3.Distance(PlayerPos.transform.position, DestinationPos.transform.position);
        //print("distance left is" + currentDistance);
       // print(/*"distance current is" + currentDistance+ */ "percent is" + percentage);
    }

}
