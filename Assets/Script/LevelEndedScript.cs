﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelEndedScript : MonoBehaviour {

    public Text scoreText;
    public Text levelNameText;


    public GameObject ScoreTxtObj;
    // Use this for initialization
    void Start () {

        
        switch (SceneManager.GetActiveScene().name)
        {
            case "Stage 1-1":
                levelNameText.text = "STAGE 1-1 DONE";
                break;
            case "Stage 1-2":
                levelNameText.text = "STAGE 1-2 DONE";
                break;
            default:
                levelNameText.text = "unknown";
                break;
        }
	}
    public void ActiveText()
    {
        StartCoroutine(TextSequence());
    }

    IEnumerator TextSequence()
    {
        levelNameText.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        scoreText.gameObject.SetActive(true);
    }

}
