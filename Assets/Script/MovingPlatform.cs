﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour {
    public GameObject plaformUp;
    public GameObject plaformDown;
    public float speed = 2200.0f;
    public string type = "UpDown";
    public bool movedown = true;
    public bool moveLeft = true;
    public bool moveBack = true;
    public GameObject GameManager;
    // Use this for initialization
    void Start () {
        GameManager = GameObject.Find("GameManager");
    }
	
	// Update is called once per frame
	void Update () {
        if (type == "UpDown")
        {
            if (movedown == true)
            {
                gameObject.transform.Translate(0.0f, -speed * (Time.deltaTime / GameManager.GetComponent<GameManager>().ObjectMovingTime), 0.0f);
            }
            else
            {
                gameObject.transform.Translate(0.0f, speed * (Time.deltaTime / GameManager.GetComponent<GameManager>().ObjectMovingTime), 0.0f);
            }

            if (gameObject.transform.position.y >= plaformUp.transform.position.y)
            {
                movedown = true;
            }
            if (gameObject.transform.position.y < plaformDown.transform.position.y)
            {
                movedown = false;
            }
        }
        else if (type == "LeftRight")
        {
            if (moveLeft == true)
            {
                gameObject.transform.Translate(-speed * (Time.deltaTime / GameManager.GetComponent<GameManager>().ObjectMovingTime), 0.0f, 0.0f);
            }
            else
            {
                gameObject.transform.Translate(speed * (Time.deltaTime / GameManager.GetComponent<GameManager>().ObjectMovingTime), 0.0f, 0.0f);
            }

            if (gameObject.transform.position.x >= plaformUp.transform.position.x)
            {
                moveLeft = true;
            }
            if (gameObject.transform.position.x < plaformDown.transform.position.x)
            {
                moveLeft = false;
            }
        }
        else {
            if (moveBack == true)
            {
                gameObject.transform.Translate(0.0f, 0.0f, -speed * (Time.deltaTime / GameManager.GetComponent<GameManager>().ObjectMovingTime));
            }
            else
            {
                gameObject.transform.Translate(0.0f, 0.0f, speed * (Time.deltaTime / GameManager.GetComponent<GameManager>().ObjectMovingTime));
            }

            if (gameObject.transform.position.z >= plaformUp.transform.position.z)
            {
                moveBack = true;
            }
            if (gameObject.transform.position.z < plaformDown.transform.position.z)
            {
                moveBack = false;
            }
        }
        
    }

    //private void OnCollisionEnter(Collision other)
    //{
    //    if (other.gameObject.tag == "Player") {
    //        other.transform.parent = transform;
    //    }
    //}

    //private void OnCollisionExit(Collision col)
    //{
    //    if (col.gameObject.tag == "Player")
    //    {
    //        col.transform.parent = null;
    //    }
    //}
}
